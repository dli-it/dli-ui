'use strict'

module.exports = (page) => {
  if (page.layout === 'landing' || page.layout === 'training-enrollment') {
    // since the component title and the page title are often the same, we use 'dli Graph Database Platform' to avoid redundancy.
    return ' - dli Graph Database Platform'
  }
  if (page && page.component && page.component.title) {
    return ` - ${page.component.title}`
  }
  return ''
}
