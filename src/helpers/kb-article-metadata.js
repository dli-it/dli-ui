module.exports = (page) => {
  const metadata = {}
  if (page.asciidoc && page.asciidoc.attributes) {
    metadata['dli-versions'] = page.asciidoc.attributes['dli-versions']
    metadata.author = page.asciidoc.attributes.author
    metadata.category = page.asciidoc.attributes.category
    metadata.tags = page.asciidoc.attributes.tags
  }
  return metadata
}
