function getTags (contentCatalog) {
  const guides = contentCatalog.getPages((page) => {
    return page.out && page.asciidoc.attributes['page-category'] === 'guide'
  })
}

